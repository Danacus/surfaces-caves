data:extend({
    {
        type = "bool-setting",
        name = "cave-darkness",
        setting_type = "runtime-global",
        default_value = true
    },
    {
        type = "bool-setting",
        name = "cave-map",
        setting_type = "startup",
        default_value = false
    }
})
